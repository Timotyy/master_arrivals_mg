Attribute VB_Name = "MasterArrival_Process_Module"
Option Explicit
Public g_strConnection As String
Public cnn As ADODB.Connection

Const g_strSMTPServer = "jca-aspera.jcatv.co.uk"
Const g_strSMTPUserName = "" ' "aria@freenetname.co.uk"
Const g_strSMTPPassword = "" '"Chorale1"
Const g_strEmailFooter = "BBC Media Portal at visualdatamedia.com"
Public g_strUserEmailAddress As String
Public g_strUserEmailName As String
Private Declare Function OpenProcess Lib "kernel32" ( _
    ByVal dwDesiredAccess As Long, ByVal bInheritHandle As Long, ByVal dwProcessId As Long) As Long
Private Declare Function CloseHandle Lib "kernel32" ( _
    ByVal hObject As Long) As Long
Private Declare Function EnumProcesses Lib "PSAPI.DLL" ( _
   lpidProcess As Long, ByVal cb As Long, cbNeeded As Long) As Long
Private Declare Function EnumProcessModules Lib "PSAPI.DLL" ( _
    ByVal hProcess As Long, lphModule As Long, ByVal cb As Long, lpcbNeeded As Long) As Long
Private Declare Function GetModuleBaseName Lib "PSAPI.DLL" Alias "GetModuleBaseNameA" ( _
    ByVal hProcess As Long, ByVal hModule As Long, ByVal lpFileName As String, ByVal nSize As Long) As Long
Private Const PROCESS_VM_READ = &H10
Private Const PROCESS_QUERY_INFORMATION = &H400
'Public Const TC_UN = 0
'Public Const TC_25 = 1
'Public Const TC_29 = 2
'Public Const TC_30 = 3
'Public Const TC_24 = 4

Public Sub MasterArrivalProcess()

Dim l_rsJobs As ADODB.Recordset, l_rsCompanies As ADODB.Recordset, l_rsMedia As ADODB.Recordset, l_strSQL As String, l_blnMediaHere As Boolean, l_rsWoolySearch As ADODB.Recordset
Dim l_strLocation As String, l_strShelf As String, l_rsTapeMovement As ADODB.Recordset, l_lngLibraryID As Long, SQL As String, l_lngEventID As Long, l_strAltLocation As String, BBCMG_OrderID As Long, BBCMG_MediaKey As String
Dim l_rstTapes As ADODB.Recordset, l_strTemp As String, l_rsTracker As ADODB.Recordset, l_lngChannelCount As Long
Dim l_rsDADCTracker As ADODB.Recordset, l_strBarcode As String, l_strEmailBody As String, l_strTapeFormat As String, l_rst As ADODB.Recordset, l_datNewTargetDate As Date, l_datNewTargetDate2 As Date
Dim l_strPathToFile As String, FSO As Scripting.FileSystemObject, Count As Long, Framerate As Integer, l_strDuration As String, l_lngDuration As Long
Dim l_rsTurnerTracker As ADODB.Recordset, l_strNewFolder As String, l_rsTurnerTracker2 As ADODB.Recordset
Dim l_lngRunningTime As Long, l_lngTurnerLibraryID As Long
Dim LockFLag As Long
Dim ArchiveBarcode As String, ArchiveLibraryID As Long, RestoreBarcode As String, RestoreLibraryID As Long
Dim l_strAudioCodec As String

cnn.Open g_strConnection

LockFLag = GetData("setting", "value", "name", "LockSystem")
If (LockFLag And 16) = 16 Then
    cnn.Close
    Exit Sub
End If

Set l_rsCompanies = New ADODB.Recordset
Set l_rsJobs = New ADODB.Recordset
Set l_rsMedia = New ADODB.Recordset
Set l_rsWoolySearch = New ADODB.Recordset
Set l_rstTapes = New ADODB.Recordset

SQL = "SELECT value FROM setting WHERE name = 'OperationsEmailAddress';"
l_rsCompanies.Open SQL, cnn, 3, 3
If l_rsCompanies.RecordCount > 0 Then
    g_strUserEmailAddress = l_rsCompanies("value")
End If
l_rsCompanies.Close
SQL = "SELECT value FROM setting WHERE name = 'OperationsEmailName';"
l_rsCompanies.Open SQL, cnn, 3, 3
If l_rsCompanies.RecordCount > 0 Then
    g_strUserEmailName = l_rsCompanies("value")
End If
l_rsCompanies.Close

'BBCMG Tape Checks
l_strSQL = "SELECT * FROM vw_BBCMG_TapeArrived;"
l_rstTapes.Open l_strSQL, cnn, 3, 3
If l_rstTapes.RecordCount > 0 Then
    Debug.Print "MasterArrivalService: BBCMG Tape Checks - " & l_rstTapes.RecordCount & " records"
    App.LogEvent "MasterArrivalService: BBCMG Tape Checks - " & l_rstTapes.RecordCount & " records"
    l_rstTapes.MoveFirst
    Do While Not l_rstTapes.EOF
        If Not IsItHere(Trim(" " & l_rstTapes("location"))) Then
            SetData "BBCMG_Preview_Tracker", "DateTapeLeft", "Preview_Order_Id", l_rstTapes("Preview_Order_Id"), FormatSQLDate(Now)
            SetData "BBCMG_Preview_Tracker", "TimeTapeLeft", "Preview_Order_Id", l_rstTapes("Preview_Order_Id"), Format(Now, "HH:NN:SS")
            If l_rstTapes("Order_Status_Id") = GetData("BBCMG_Order_Status", "Order_Status_Id", "Order_Status", "Tape Arrived") Then
                SetData "BBCMG_Preview_Order", "Order_Status_Id", "Preview_Order_Id", l_rstTapes("Preview_Order_Id"), GetData("BBCMG_Order_Status", "Order_Status_Id", "Order_Status", "Waiting For Tape")
            End If
            SetData "BBCMG_Preview_Tracker", "TapeLocation", "Preview_order_Id", l_rstTapes("Preview_Order_Id"), l_rstTapes("location")
            SetData "BBCMG_Preview_Tracker", "TapeShelf", "Preview_Order_Id", l_rstTapes("Preview_Order_Id"), l_rstTapes("shelf")
        End If
        l_rstTapes.MoveNext
    Loop
End If
l_rstTapes.Close

'BBCMG File Checks - Newly Arrived files - move them to the LAT-CETA
l_strEmailBody = ""
l_strSQL = "SELECT * FROM vw_BBCMG_WaitingForFile;"
l_rstTapes.Open l_strSQL, cnn, 3, 3
If l_rstTapes.RecordCount > 0 Then
    l_rstTapes.MoveFirst
    App.LogEvent "MasterarrivalService checking for arrival of " & l_rstTapes.RecordCount & " BBCMG files"
    Debug.Print l_rstTapes.RecordCount & " files BBCMG waiting to check"
    Do While Not l_rstTapes.EOF
        If GetDataSQL("SELECT eventID FROM vw_Events_On_Discstore WHERE companyID = 924 AND system_deleted = 0 AND bigfilesize IS NOT NULL AND clipformat IS NOT NULL AND clipfilename = '" & _
        QuoteSanitise(l_rstTapes("Tape_Number")) & _
        "' AND altlocation  <> '__TranscodeResults\AutoDelete7\AS11-Transcode' AND altlocation <> '924\IngestFiles' AND altlocation <> '924\AutoDelete3' AND LibraryID NOT IN (712787, 712856, 712858, 712859, 712857, 712893, 740691, 612748);") <> 0 Then
            l_lngEventID = GetDataSQL("SELECT eventID FROM vw_Events_On_Discstore WHERE companyID = 924 AND system_deleted = 0 AND bigfilesize IS NOT NULL AND clipformat IS NOT NULL AND clipfilename = '" & _
            QuoteSanitise(l_rstTapes("Tape_Number")) & _
            "' AND altlocation  <> '__TranscodeResults\AutoDelete7\AS11-Transcode' AND altlocation <> '924\IngestFiles' AND altlocation <> '924\AutoDelete3' AND LibraryID NOT IN (712787, 712856, 712858, 712859, 712857, 712893, 740691, 612748);")
            If GetData("events", "clipformat", "eventID", l_lngEventID) = "Other" Then
                l_strSQL = "INSERT INTO event_file_request (eventID, event_file_Request_typeID, RequestName, RequesterEmail) VALUES ("
                l_strSQL = l_strSQL & l_lngEventID & ", "
                l_strSQL = l_strSQL & GetData("event_file_request_type", "event_file_request_typeID", "description", "MediaInfo + TC") & ", "
                l_strSQL = l_strSQL & "'MastArriv', 'uk.ops-VIE@visualdatamedia.com');"
                cnn.Execute l_strSQL
                Exit Do
            End If
            l_strAltLocation = GetData("events", "altlocation", "eventID", l_lngEventID)
            l_lngChannelCount = Val(Trim(" " & GetData("events", "channelcount", "eventID", l_lngEventID)))
            l_strAudioCodec = GetData("events", "clipaudiocodec", "eventID", l_lngEventID)
            If GetData("events", "clipformat", "eventID", l_lngEventID) = "Quicktime" Then
                If GetData("events", "clipcodec", "eventID", l_lngEventID) = "ProRes HQ" _
                Or ((l_strAltLocation = "Aspera-BBCMG-SPORT" Or l_strAltLocation = "Aspera-BBCMG-NACA" Or LCase(l_strAltLocation) = "924\sq_for_ingest") _
                And (GetData("events", "clipcodec", "eventID", l_lngEventID) = "ProRes HQ" Or GetData("events", "clipcodec", "eventID", l_lngEventID) = "ProRes SQ")) _
                Then
                    If l_strAudioCodec = "aes3" Then
                        If GetData("events", "bigfilesize", "eventID", l_lngEventID) <> GetData("BBCMG_Preview_Tracker", "bigfilesize", "Preview_Order_ID", l_rstTapes("Preview_Order_ID")) Then
                            l_strEmailBody = l_strEmailBody & "Audio codec aes3 is unsupported. Please supply a replacement file" & vbCrLf & "File reports that it has aes3 audio - this is not supported." & vbCrLf
                            SetData "BBCMG_Preview_Tracker", "bigfilesize", "Preview_Order_ID", l_rstTapes("preview_Order_ID"), GetData("events", "bigfilesize", "eventID", l_lngEventID)
                            SetData "BBCMG_Preview_Tracker", "fileerrorreported", "Preview_Order_ID", l_rstTapes("preview_Order_ID"), 1
                            SetData "events", "eventdate", "eventID", l_lngEventID, Format(Now, "YYYY-MM-DD HH:NN:SS")
                        End If
                    Else
                        If (l_lngChannelCount = 4 Or l_lngChannelCount = 2 Or l_lngChannelCount = 1 Or l_lngChannelCount = 0 Or l_lngChannelCount = 12 Or l_lngChannelCount = 16 Or l_lngChannelCount = 8) _
                        Or (l_strAltLocation = "Aspera-BBCMG-NACA" And (l_lngChannelCount = 4 Or l_lngChannelCount = 2 Or l_lngChannelCount = 1 Or l_lngChannelCount = 0)) _
                        Or (l_strAltLocation = "Aspera-BBCMG-SPORT") Or (Left(LCase(l_strAltLocation), 17) = "924\sq_for_ingest") _
                        Then
                            Select Case Trim(" " & GetData("events", "clipframerate", "eventID", l_lngEventID))
                                Case "23.98", "24"
                                    Framerate = TC_24
                                Case "25"
                                    Framerate = TC_25
                                Case "29.97"
                                    Framerate = TC_29
                                Case "30", "29.97 NDF"
                                    Framerate = TC_30
                                Case "50"
                                    Framerate = TC_50
                                Case "59.94"
                                    Framerate = TC_59
                                Case "60"
                                    Framerate = TC_60
                                Case Else
                                    Framerate = TC_UN
                            End Select
                            If FrameNumber(Trim(" " & GetData("events", "timecodestart", "eventID", l_lngEventID)), Framerate) < FrameNumber(Trim(" " & GetData("events", "timecodestop", "eventID", l_lngEventID)), Framerate) Then
                                l_lngLibraryID = GetData("events", "libraryID", "eventID", l_lngEventID)
                                BBCMG_OrderID = l_rstTapes("Preview_Order_Id")
                                BBCMG_MediaKey = GetData("BBCMG_Media_Key", "Media_Key", "Media_ID", QuoteSanitise(l_rstTapes("Tape_Number")))
                                BBCMG_MediaKey = Mid(BBCMG_MediaKey, 2, Len(BBCMG_MediaKey) - 2)
                                If GetDataSQL("SELECT transcoderequestID FROM transcoderequest WHERE destination1clipID = " & l_lngEventID & " OR destination2clipID = " & l_lngEventID & " OR destination3clipID = " & l_lngEventID & _
                                " OR destination4clipID = " & l_lngEventID & " OR destination5clipID = " & l_lngEventID & ";") <> "" Then
                                    SetData "BBCMG_Preview_Tracker", "WasTranscodedFromMXF", "Preview_Order_ID", BBCMG_OrderID, 1
                                End If
                                l_strPathToFile = GetData("library", "subtitle", "libraryID", GetData("events", "libraryID", "eventID", l_lngEventID))
                                Set FSO = New Scripting.FileSystemObject
                                If FSO.FileExists(l_strPathToFile & "\" & GetData("events", "altlocation", "eventID", l_lngEventID) & "\" & l_rstTapes("Tape_Number")) Then
                                  On Error GoTo FILEALREADYEXISTS
                                  FSO.MoveFile l_strPathToFile & "\" & GetData("events", "altlocation", "eventID", l_lngEventID) & "\" & l_rstTapes("Tape_Number"), l_strPathToFile & "\" & GetData("events", "altlocation", "eventID", l_lngEventID) & "\" & BBCMG_MediaKey & ".mov"
                                  On Error GoTo 0
                                End If
                                l_strPathToFile = l_strPathToFile & "\" & GetData("events", "altlocation", "eventID", l_lngEventID)
                                Set FSO = Nothing
                                SetData "events", "clipreference", "eventID", l_lngEventID, BBCMG_MediaKey
                                SetData "events", "clipfilename", "eventID", l_lngEventID, BBCMG_MediaKey & ".mov"
                                SetData "events", "eventtitle", "eventID", l_lngEventID, QuoteSanitise(GetData("BBCMG_Preview_Order", "Program_Title", "Preview_Order_ID", BBCMG_OrderID))
                                SetData "events", "customfield1", "eventID", l_lngEventID, QuoteSanitise(GetData("BBCMG_Preview_Order", "Program_ID", "Preview_Order_ID", BBCMG_OrderID))
                                SetData "events", "customfield2", "eventID", l_lngEventID, GetData("BBCMG_Preview_Order", "TX_Date", "Preview_Order_ID", BBCMG_OrderID)
                                SetData "events", "customfield3", "eventID", l_lngEventID, GetData("BBCMG_Preview_Order", "Request_Number", "Preview_Order_ID", BBCMG_OrderID)
                                SetData "BBCMG_Preview_Tracker", "DateTapeArrived", "Preview_Order_ID", BBCMG_OrderID, FormatSQLDate(Now)
                                SetData "BBCMG_Preview_Tracker", "TimeTapeArrived", "Preview_Order_ID", BBCMG_OrderID, Format(Now, "hh:nn:ss")
                                SetData "BBCMG_Preview_Tracker", "DateIngested", "Preview_Order_ID", BBCMG_OrderID, Format(Now, "yyyy-mm-dd")
                                SetData "BBCMG_Preview_Tracker", "TimeIngested", "Preview_Order_ID", BBCMG_OrderID, Format(Now, "hh:nn:ss")
                                SetData "BBCMG_Preview_Order", "Order_Status_ID", "Preview_Order_ID", BBCMG_OrderID, GetData("BBCMG_Order_Status", "Order_Status_ID", "Order_Status", "Tape Arrived")
                                l_strEmailBody = "File was accepted and is proceeding into the workflow"
                                ArchiveBarcode = GetData("setting", "value", "name", "BBCMG_Archive")
                                ArchiveLibraryID = GetData("library", "libraryID", "barcode", ArchiveBarcode)
                                SQL = "INSERT INTO event_file_request(eventID, SourceLibraryID, event_file_request_typeID, NewLibraryID, NewAltLocation, bigfilesize, RequestName, Urgent) VALUES ("
                                SQL = SQL & l_lngEventID & ", "
                                SQL = SQL & l_lngLibraryID & ", "
                                SQL = SQL & GetData("event_file_request_type", "event_file_request_typeID", "description", "Move") & ", "
                                SQL = SQL & ArchiveLibraryID & ", "
                                SQL = SQL & "'924\" & Left(BBCMG_MediaKey, 1) & "', "
                                SQL = SQL & GetData("events", "bigfilesize", "eventID", l_lngEventID) & ", "
                                SQL = SQL & "'BBCMG', 1);"
                                Debug.Print SQL
                                cnn.Execute SQL
                            ElseIf GetData("events", "timecodestart", "eventID", l_lngEventID) = "" Or GetData("events", "timecodestop", "eventID", l_lngEventID) = "" Then
                                l_strSQL = "INSERT INTO event_file_request (eventID, event_file_Request_typeID, RequestName, RequesterEmail) VALUES ("
                                l_strSQL = l_strSQL & l_lngEventID & ", "
                                l_strSQL = l_strSQL & GetData("event_file_request_type", "event_file_request_typeID", "description", "MediaInfo + TC") & ", "
                                l_strSQL = l_strSQL & "'MastArriv', 'uk.ops-VIE@visualdatamedia.com');"
                                cnn.Execute l_strSQL
                                Exit Do
                            Else
                                If GetData("events", "bigfilesize", "eventID", l_lngEventID) <> GetData("BBCMG_Preview_Tracker", "bigfilesize", "Preview_Order_ID", l_rstTapes("Preview_Order_ID")) Then
                                    If Val(GetData("tracker_item", "Tracker_itemID", "originaleventID", l_lngEventID)) = 0 Then
                                        SQL = "INSERT INTO tracker_item (companyID, WorkflowVariantID, itemreference, itemfilename, originaleventID) VALUES ("
                                        SQL = SQL & "924, 149, "
                                        SQL = SQL & "'" & QuoteSanitise(GetData("events", "clipreference", "eventID", l_lngEventID)) & "', "
                                        SQL = SQL & "'" & QuoteSanitise(GetData("events", "cliprefilename", "eventID", l_lngEventID)) & "', "
                                        SQL = SQL & l_lngEventID & ");"
                                        
                                        Debug.Print SQL
                                        cnn.Execute SQL
                                    End If
                                    l_strEmailBody = l_strEmailBody & "Timecode track invalid - timecode cannot pass through midnight." & vbCrLf
                                    SetData "BBCMG_Preview_Tracker", "bigfilesize", "Preview_Order_ID", l_rstTapes("preview_Order_ID"), GetData("events", "bigfilesize", "eventID", l_lngEventID)
                                    SetData "BBCMG_Preview_Tracker", "fileerrorreported", "Preview_Order_ID", l_rstTapes("preview_Order_ID"), 1
                                    SetData "events", "eventdate", "eventID", l_lngEventID, Format(Now, "YYYY-MM-DD HH:NN:SS")
                                End If
                            End If
                        Else
                            If GetData("events", "bigfilesize", "eventID", l_lngEventID) <> GetData("BBCMG_Preview_Tracker", "bigfilesize", "Preview_Order_ID", l_rstTapes("Preview_Order_ID")) Then
                                If l_strAltLocation = "Aspera-BBCMG" Then
                                    l_strEmailBody = l_strEmailBody & "Audio Channel Count is not valid - should be 2 or 4 channels" & vbCrLf & "File reports that it has " & GetData("events", "channelcount", "eventID", l_lngEventID) & " channels" & vbCrLf
                                Else
                                    l_strEmailBody = l_strEmailBody & "Audio Channel Count is not valid - should be 0, 1, 2 or 4 channels" & vbCrLf & "File reports that it has " & GetData("events", "channelcount", "eventID", l_lngEventID) & " channels" & vbCrLf
                                End If
                                SetData "BBCMG_Preview_Tracker", "bigfilesize", "Preview_Order_ID", l_rstTapes("preview_Order_ID"), GetData("events", "bigfilesize", "eventID", l_lngEventID)
                                SetData "BBCMG_Preview_Tracker", "fileerrorreported", "Preview_Order_ID", l_rstTapes("preview_Order_ID"), 1
                                SetData "events", "eventdate", "eventID", l_lngEventID, Format(Now, "YYYY-MM-DD HH:NN:SS")
                            End If
                        End If
                    End If
                ElseIf GetData("events", "clipcodec", "eventID", l_lngEventID) = "ProRes SQ" Then
                    If GetData("events", "bigfilesize", "eventID", l_lngEventID) <> GetData("BBCMG_Preview_Tracker", "bigfilesize", "Preview_Order_ID", l_rstTapes("Preview_Order_ID")) Then
'                        If Val(GetData("tracker_item", "Tracker_itemID", "originaleventID", l_lngEventID)) = 0 Then
'                            SQL = "INSERT INTO tracker_item (companyID, WorkflowVariantID, itemreference, itemfilename, originaleventID) VALUES ("
'                            SQL = SQL & "924, 150, "
'                            SQL = SQL & "'" & QuoteSanitise(GetData("events", "clipreference", "eventID", l_lngEventID)) & "', "
'                            SQL = SQL & "'" & QuoteSanitise(GetData("events", "cliprefilename", "eventID", l_lngEventID)) & "', "
'                            SQL = SQL & l_lngEventID & ");"
'
'                            Debug.Print SQL
'                            cnn.Execute SQL
'                        End If
                        l_strEmailBody = l_strEmailBody & "File has come in as ProResSQ. Files should be ProRes HQ." & vbCrLf
                        'l_strEmailBody = l_strEmailBody & "File has come in as ProResSQ. It is being Transcoded to ProResSQ befoew ingest." & vbCrLf
                        If FrameNumber(Trim(" " & GetData("events", "timecodestart", "eventID", l_lngEventID)), Framerate) < FrameNumber(Trim(" " & GetData("events", "timecodestop", "eventID", l_lngEventID)), Framerate) Then
                            If Val(GetData("tracker_item", "Tracker_itemID", "originaleventID", l_lngEventID)) = 0 Then
                                SQL = "INSERT INTO tracker_item (companyID, WorkflowVariantID, itemreference, itemfilename, originaleventID) VALUES ("
                                SQL = SQL & "924, 149, "
                                SQL = SQL & "'" & QuoteSanitise(GetData("events", "clipreference", "eventID", l_lngEventID)) & "', "
                                SQL = SQL & "'" & QuoteSanitise(GetData("events", "cliprefilename", "eventID", l_lngEventID)) & "', "
                                SQL = SQL & l_lngEventID & ");"
                                Debug.Print SQL
                                cnn.Execute SQL
                            End If
                        End If
                        SetData "BBCMG_Preview_Tracker", "bigfilesize", "Preview_Order_ID", l_rstTapes("preview_Order_ID"), GetData("events", "bigfilesize", "eventID", l_lngEventID)
                        SetData "BBCMG_Preview_Tracker", "fileerrorreported", "Preview_Order_ID", l_rstTapes("preview_Order_ID"), 1
                        SetData "events", "eventdate", "eventID", l_lngEventID, Format(Now, "YYYY-MM-DD HH:NN:SS")
                    End If
                Else
                    If GetData("events", "bigfilesize", "eventID", l_lngEventID) <> GetData("BBCMG_Preview_Tracker", "bigfilesize", "Preview_Order_ID", l_rstTapes("Preview_Order_ID")) Then
                        l_strEmailBody = l_strEmailBody & "Codec was not valid - should be ProRes HQ" & vbCrLf & "File reports that it is " & GetData("events", "clipcodec", "eventID", l_lngEventID) & vbCrLf
                        SetData "BBCMG_Preview_Tracker", "bigfilesize", "Preview_Order_ID", l_rstTapes("preview_Order_ID"), GetData("events", "bigfilesize", "eventID", l_lngEventID)
                        SetData "BBCMG_Preview_Tracker", "fileerrorreported", "Preview_Order_ID", l_rstTapes("preview_Order_ID"), 1
                        SQL = "INSERT INTO event_file_request(eventID, SourceLibraryID, event_file_request_typeID, NewLibraryID, NewAltLocation, bigfilesize, RequestName, Urgent) VALUES ("
                        SQL = SQL & l_lngEventID & ", "
                        SQL = SQL & GetData("events", "libraryID", "eventID", l_lngEventID) & ", "
                        SQL = SQL & GetData("event_file_request_type", "event_file_request_typeID", "description", "Move") & ", "
                        SQL = SQL & GetData("events", "libraryID", "eventID", l_lngEventID) & ", "
                        SQL = SQL & "'924\AutoDelete3\Rejects', "
                        SQL = SQL & GetData("events", "bigfilesize", "eventID", l_lngEventID) & ", "
                        SQL = SQL & "'BBCMG', 1);"
                        Debug.Print SQL
                        cnn.Execute SQL
                        SetData "events", "eventdate", "eventID", l_lngEventID, Format(Now, "YYYY-MM-DD HH:NN:SS")
                    End If
                End If
            Else
                If GetData("events", "bigfilesize", "eventID", l_lngEventID) <> GetData("BBCMG_Preview_Tracker", "bigfilesize", "Preview_Order_ID", l_rstTapes("Preview_Order_ID")) Then
                    l_strEmailBody = l_strEmailBody & "File type was not valid - should be a Quicktime .mov file" & vbCrLf & "File reports that it is " & GetData("events", "clipformat", "eventID", l_lngEventID) & vbCrLf
                    SetData "BBCMG_Preview_Tracker", "bigfilesize", "Preview_Order_ID", l_rstTapes("preview_Order_ID"), GetData("events", "bigfilesize", "eventID", l_lngEventID)
                    SetData "BBCMG_Preview_Tracker", "fileerrorreported", "Preview_Order_ID", l_rstTapes("preview_Order_ID"), 1
                    SQL = "INSERT INTO event_file_request(eventID, SourceLibraryID, event_file_request_typeID, NewLibraryID, NewAltLocation, bigfilesize, RequestName, Urgent) VALUES ("
                    SQL = SQL & l_lngEventID & ", "
                    SQL = SQL & GetData("events", "libraryID", "eventID", l_lngEventID) & ", "
                    SQL = SQL & GetData("event_file_request_type", "event_file_request_typeID", "description", "Move") & ", "
                    SQL = SQL & GetData("events", "libraryID", "eventID", l_lngEventID) & ", "
                    SQL = SQL & "'924\AutoDelete3\Rejects', "
                    SQL = SQL & GetData("events", "bigfilesize", "eventID", l_lngEventID) & ", "
                    SQL = SQL & "'BBCMG', 1);"
                    Debug.Print SQL
                    cnn.Execute SQL
                    SetData "events", "eventdate", "eventID", l_lngEventID, Format(Now, "YYYY-MM-DD HH:NN:SS")
                End If
            End If
        Else
            BBCMG_MediaKey = GetData("BBCMG_Media_Key", "Media_Key", "Media_ID", QuoteSanitise(l_rstTapes("Tape_Number")))
            BBCMG_MediaKey = Mid(BBCMG_MediaKey, 2, Len(BBCMG_MediaKey) - 2)
            If GetDataSQL("SELECT eventID FROM vw_Events_On_Discstore WHERE companyID = 924 AND system_deleted = 0 AND bigfilesize IS NOT NULL AND clipfilename = '" & BBCMG_MediaKey & ".mov' AND eventID NOT IN (SELECT eventID FROM vw_BBCMG_WaitingForFileToMove);") <> 0 Then
                l_lngEventID = GetDataSQL("SELECT eventID FROM vw_Events_On_Discstore WHERE companyID = 924 AND system_deleted = 0 AND bigfilesize IS NOT NULL AND clipfilename = '" & BBCMG_MediaKey & ".mov' AND eventID NOT IN (SELECT eventID FROM vw_BBCMG_WaitingForFileToMove);")
                BBCMG_OrderID = l_rstTapes("Preview_Order_Id")
                SetData "BBCMG_Preview_Tracker", "DateTapeArrived", "Preview_Order_ID", BBCMG_OrderID, FormatSQLDate(Now)
                SetData "BBCMG_Preview_Tracker", "TimeTapeArrived", "Preview_Order_ID", BBCMG_OrderID, Format(Now, "hh:nn:ss")
                SetData "BBCMG_Preview_Tracker", "DateIngested", "Preview_Order_ID", BBCMG_OrderID, Format(Now, "yyyy-mm-dd")
                SetData "BBCMG_Preview_Tracker", "TimeIngested", "Preview_Order_ID", BBCMG_OrderID, Format(Now, "hh:nn:ss")
                SetData "BBCMG_Preview_Order", "Order_Status_ID", "Preview_Order_ID", BBCMG_OrderID, GetData("BBCMG_Order_Status", "Order_Status_ID", "Order_Status", "Tape Arrived")
                ArchiveBarcode = GetData("setting", "value", "name", "BBCMG_Archive")
                ArchiveLibraryID = GetData("library", "libraryID", "barcode", ArchiveBarcode)
                RestoreBarcode = GetData("setting", "value", "name", "BBCMGRestoreLocationBarcode")
                RestoreLibraryID = GetData("library", "libraryID", "barcode", RestoreBarcode)
                l_lngLibraryID = GetData("events", "libraryID", "eventID", l_lngEventID)
                If (GetData("events", "libraryID", "eventID", l_lngEventID) <> ArchiveLibraryID And GetData("events", "libraryID", "eventID", l_lngEventID) <> RestoreLibraryID) _
                Or InStr(UCase(GetData("events", "altlocation", "eventID", l_lngEventID)), "AUTODELETE") > 0 _
                Or UCase(GetData("events", "altlocation", "eventID", l_lngEventID)) = UCase("924\ProResHQ_Transcoded_From_MXF") Then
                    If GetDataSQL("SELECT event_file_requestID FROM event_file_request WHERE eventID = " & l_lngEventID & " AND (RequestStarted IS NULL OR (RequestStarted IS NOT NULL AND (RequestComplete IS NOT NULL OR RequestFailed IS NOT NULL)))") <> "" Then
                        SQL = "INSERT INTO event_file_request(eventID, SourceLibraryID, event_file_request_typeID, NewLibraryID, NewAltLocation, bigfilesize, RequestName, Urgent) VALUES ("
                        SQL = SQL & l_lngEventID & ", "
                        SQL = SQL & l_lngLibraryID & ", "
                        SQL = SQL & GetData("event_file_request_type", "event_file_request_typeID", "description", "Move") & ", "
                        SQL = SQL & ArchiveLibraryID & ", "
                        SQL = SQL & "'924\" & Left(BBCMG_MediaKey, 1) & "', "
                        SQL = SQL & GetData("events", "bigfilesize", "eventID", l_lngEventID) & ", "
                        SQL = SQL & "'BBCMG', 1);"
                        Debug.Print SQL
                        cnn.Execute SQL
                    Else
                        SetData "BBCMG_Preview_Order", "Order_Status_ID", "Preview_Order_ID", BBCMG_OrderID, GetData("BBCMG_Order_Status", "Order_Status_ID", "Order_Status", "Ingest Completed")
                    End If
                Else
                    SetData "BBCMG_Preview_Order", "Order_Status_ID", "Preview_Order_ID", BBCMG_OrderID, GetData("BBCMG_Order_Status", "Order_Status_ID", "Order_Status", "Ingest Completed")
                End If
            End If
        End If
        
        If l_strEmailBody <> "" Then
            If l_strEmailBody <> "File was accepted and is proceeding into the workflow" Then
                If l_strEmailBody = "File was Uploaded, and Transcoded, but a Target File already exists. New File has not been accepted" Then
                    l_strPathToFile = GetData("library", "subtitle", "libraryID", GetData("events", "libraryID", "eventID", l_lngEventID))
                    l_strPathToFile = l_strPathToFile & "\" & GetData("events", "altlocation", "eventID", l_lngEventID) & "\" & l_rstTapes("Tape_Number")
                    l_strEmailBody = "File was Uploaded, and Transcoded, but a Target File already exists. New File has not been accepted." & vbCrLf & vbCrLf & "The file was: " & l_rstTapes("Tape_Number") & vbCrLf
                    SendSMTPMail "bbcmg-VIE@visualdatamedia.com", "", "BBCMG Uploaded file to VDMS Rejected", l_strEmailBody, "", True, "WWEditVillageSupport@bbc.com;wwnetopsmiami@bbc.com", "", "thart@visualdatamedia.com"
                    l_strEmailBody = ""
                Else
                    l_strPathToFile = GetData("library", "subtitle", "libraryID", GetData("events", "libraryID", "eventID", l_lngEventID))
                    l_strPathToFile = l_strPathToFile & "\" & GetData("events", "altlocation", "eventID", l_lngEventID) & "\" & l_rstTapes("Tape_Number")
                    l_strEmailBody = "A file uploaded to VDMS for BBCMG has been found to not conform to the agreed specification for BBCMG Masterfiles" & vbCrLf & vbCrLf & "The file was: " & l_rstTapes("Tape_Number") & vbCrLf & l_strEmailBody
                    SendSMTPMail "bbcmg-VIE@visualdatamedia.com", "", "BBCMG Uploaded file to VDMS Rejected", l_strEmailBody, "", True, "WWEditVillageSupport@bbc.com;wwnetopsmiami@bbc.com", "", "thart@visualdatamedia.com"
                    l_strEmailBody = ""
                End If
            ElseIf l_strEmailBody = "File was accepted and is proceeding into the workflow" Then
                l_strPathToFile = GetData("library", "subtitle", "libraryID", GetData("events", "libraryID", "eventID", l_lngEventID))
                l_strPathToFile = l_strPathToFile & "\" & GetData("events", "altlocation", "eventID", l_lngEventID) & "\" & l_rstTapes("Tape_Number")
                l_strEmailBody = "A file uploaded to VDMS for BBCMG has been checked in and has now entered the workflow as a BBCMG Masterfile" & vbCrLf & vbCrLf & "The file was: " & l_rstTapes("Tape_Number") & vbCrLf
                SendSMTPMail "bbcmg-VIE@visualdatamedia.com", "", "BBCMG Uploaded file to VDMS Accepted", l_strEmailBody, "", True, "WWEditVillageSupport@bbc.com;wwnetopsmiami@bbc.com", "", "thart@visualdatamedia.com"
                l_strEmailBody = ""
            End If
        End If
RESUMEPOINT1:
        l_rstTapes.MoveNext
    Loop
End If
l_rstTapes.Close

'BBCMG File Checks - Files that have moved to BFS-CETA
l_strEmailBody = ""
l_strSQL = "SELECT * FROM vw_BBCMG_WaitingForFileToMove;"
l_rstTapes.Open l_strSQL, cnn, 3, 3
If l_rstTapes.RecordCount > 0 Then
    l_rstTapes.MoveFirst
    App.LogEvent "MasterarrivalService checking for arrival of " & l_rstTapes.RecordCount & " BBCMG files"
    Debug.Print l_rstTapes.RecordCount & " files BBCMG waiting to check"
    Do While Not l_rstTapes.EOF
        BBCMG_MediaKey = GetData("BBCMG_Media_Key", "Media_Key", "Media_ID", QuoteSanitise(l_rstTapes("Tape_Number")))
        BBCMG_MediaKey = Mid(BBCMG_MediaKey, 2, Len(BBCMG_MediaKey) - 2)
        BBCMG_OrderID = l_rstTapes("Preview_Order_Id")
        ArchiveBarcode = GetData("setting", "value", "name", "BBCMG_Archive")
        ArchiveLibraryID = GetData("library", "libraryID", "barcode", ArchiveBarcode)
        If GetDataSQL("SELECT eventID FROM vw_Events_On_Discstore WHERE companyID = 924 AND system_deleted = 0 AND clipfilename = '" & BBCMG_MediaKey & ".mov' AND libraryID = " & ArchiveLibraryID & " AND bigfilesize IS NOT NULL;") <> "" Then
            SetData "BBCMG_Preview_Order", "Order_Status_ID", "Preview_Order_ID", BBCMG_OrderID, GetData("BBCMG_Order_Status", "Order_Status_ID", "Order_Status", "Ingest Completed")
        End If
        l_rstTapes.MoveNext
    Loop
End If
l_rstTapes.Close

'BBCMG Unwanted File Checks
l_strEmailBody = ""
l_strSQL = "SELECT * FROM vw_BBCMG_Unexpected_Files where bigfilesize IS NOT NULL;"
l_rstTapes.Open l_strSQL, cnn, 3, 3
If l_rstTapes.RecordCount > 0 Then
    l_rstTapes.MoveFirst
    App.LogEvent "MasterarrivalService checking unexpected " & l_rstTapes.RecordCount & " BBCMG files"
    Debug.Print l_rstTapes.RecordCount & " unexpected files BBCMG waiting to check"
    Do While Not l_rstTapes.EOF
        l_strEmailBody = "A file has been sent to VDMS under one of the BBCMG accounts, but no BBCMG order exists for it." & vbCrLf & vbCrLf
        l_strEmailBody = l_strEmailBody & "Filename: " & l_rstTapes("clipfilename") & vbCrLf & vbCrLf
        l_strEmailBody = l_strEmailBody & "If no order is received for this file, it will be automatically deleted in 7 days." & vbCrLf
        SendSMTPMail "bbcmg-VIE@visualdatamedia.com", "", "Unexpected BBCMG file uploaded to VDMS", l_strEmailBody, "", True, "wwmg.operations@bbc.com;WWEditVillageSupport@bbc.com;MG.Tech.Team.Planner@bbc.com;MediaHubTech@bbc.co.uk", "", "thart@visualdatamedia.com"
        l_strEmailBody = ""
        l_rstTapes("eventdate") = Now
        l_rstTapes.Update
'        ArchiveBarcode = GetData("setting", "value", "name", "BBCMG_Archive")
'        ArchiveLibraryID = GetData("library", "libraryID", "barcode", ArchiveBarcode)
'        l_lngLibraryID = GetData("events", "libraryID", "eventID", l_rstTapes("eventID"))
'        SQL = "INSERT INTO event_file_request(eventID, SourceLibraryID, event_file_request_typeID, NewLibraryID, NewAltLocation, bigfilesize, RequestName) VALUES ("
'        SQL = SQL & l_rstTapes("eventID") & ", "
'        SQL = SQL & l_lngLibraryID & ", "
'        SQL = SQL & GetData("event_file_request_type", "event_file_request_typeID", "description", "Move") & ", "
'        SQL = SQL & ArchiveLibraryID & ", "
'        SQL = SQL & "'924\AutoDelete7', "
'        SQL = SQL & l_rstTapes("bigfilesize") & ", "
'        SQL = SQL & "'BBCMG');"
'        Debug.Print SQL
'        cnn.Execute SQL
        l_rstTapes.MoveNext
    Loop
End If
l_rstTapes.Close

cnn.Close
Exit Sub

FILEALREADYEXISTS:
l_strEmailBody = "File was Uploaded, and Transcoded, but a Target File already exists. New File has not been accepted"
FSO.MoveFile l_strPathToFile & "\" & GetData("events", "altlocation", "eventID", l_lngEventID) & "\" & l_rstTapes("Tape_Number"), l_strPathToFile & "\924\AutoDele3\" & l_rstTapes("Tape_Number")
Resume RESUMEPOINT1

End Sub

Private Function IsProcessRunning(ByVal sProcess As String) As Boolean

Const MAX_PATH As Long = 260

Dim lProcesses() As Long, lModules() As Long, N As Long, lRet As Long, hProcess As Long
Dim sName As String
        
sProcess = UCase$(sProcess)
ReDim lProcesses(1023) As Long
If EnumProcesses(lProcesses(0), 1024 * 4, lRet) Then
    For N = 0 To (lRet \ 4) - 1
        hProcess = OpenProcess(PROCESS_QUERY_INFORMATION Or PROCESS_VM_READ, 0, lProcesses(N))
        If hProcess Then
            ReDim lModules(1023)
            If EnumProcessModules(hProcess, lModules(0), 1024 * 4, lRet) Then
                sName = String$(MAX_PATH, vbNullChar)
                GetModuleBaseName hProcess, lModules(0), sName, MAX_PATH
                sName = Left$(sName, InStr(sName, vbNullChar) - 1)
                If Len(sName) = Len(sProcess) Then
                    If sProcess = UCase$(sName) Then IsProcessRunning = True: Exit Function
                End If
            End If
        End If
        CloseHandle hProcess
    Next N
End If
End Function

Function fb_bst(pd_dt As Date) As Boolean
  Const cs_fac = "%fb_bst, "
  Dim ls_eom As String
  If Not IsDate(pd_dt) Then fb_bst = False: Exit Function
  Select Case Month(pd_dt)
  Case 4, 5, 6, 7, 8, 9
    fb_bst = True
  Case 1, 2, 11, 12
    fb_bst = False
  Case 3
    fb_bst = False
    If Day(pd_dt) < 25 Then Exit Function
    ls_eom = "31/03/" & Year(pd_dt)
    If Weekday(pd_dt) - Weekday(ls_eom) > 0 Then Exit Function
    If Hour(pd_dt) < 1 Then Exit Function
    fb_bst = True
  Case 10
    fb_bst = True
    If Day(pd_dt) < 25 Then Exit Function
    ls_eom = "31/10/" & Year(pd_dt)
    If Weekday(pd_dt) - Weekday(ls_eom) > 0 Then Exit Function
'    If Hour( pd_dt ) < 1 Then Exit Function
    fb_bst = False
  End Select
End Function

Function GetData(lp_strTableName As String, lp_strFieldToReturn As String, lp_strFieldToSearch As String, lp_varCriterea As Variant) As Variant
        '<EhHeader>
        On Error GoTo GetData_Err
        '</EhHeader>


    Dim l_strSQL As String
    l_strSQL = "SELECT " & lp_strFieldToReturn & " FROM " & lp_strTableName & " WHERE " & lp_strFieldToSearch & " = '" & (lp_varCriterea) & "'"

    Dim l_rstGetData As New ADODB.Recordset
    Dim l_con As New ADODB.Connection

    l_con.Open g_strConnection
    l_rstGetData.Open l_strSQL, l_con, adOpenStatic, adLockReadOnly

    If Not l_rstGetData.EOF Then
    
        If Not IsNull(l_rstGetData(lp_strFieldToReturn)) Then
            GetData = l_rstGetData(lp_strFieldToReturn)
            GoTo Proc_CloseDB
        End If

    Else
        GoTo Proc_CloseDB

    End If

    Select Case l_rstGetData.Fields(lp_strFieldToReturn).Type
    Case adChar, adVarChar, adVarWChar, 201, 203
        GetData = ""
    Case adBigInt, adBinary, adBoolean, adCurrency, adDecimal, adDouble, adInteger
        GetData = 0
    Case adDate, adDBDate, adDBTime, adDBTimeStamp
        GetData = 0
    Case Else
        GetData = False
    End Select

Proc_CloseDB:

    On Error Resume Next

    l_rstGetData.Close
    Set l_rstGetData = Nothing

    l_con.Close
    Set l_con = Nothing


        '<EhFooter>
        Exit Function

GetData_Err:
        App.LogEvent Err.Description & vbCrLf & _
               "in prjJCAFileManager.modTest.GetData " & _
               "at line " & Erl
        Resume Next
        '</EhFooter>
End Function

Function SetData(lp_strTableName As String, lp_strFieldToEdit As String, lp_strFieldToSearch As String, lp_varCriterea As Variant, lp_varValue As Variant) As Variant

Dim l_strSQL As String, l_UpdateValue As String, c As ADODB.Connection

If UCase(lp_varValue) = "NULL" Or lp_varValue = "" Then
    l_UpdateValue = "NULL"
Else
    l_UpdateValue = "'" & lp_varValue & "'"
End If

l_strSQL = "UPDATE " & lp_strTableName & " SET " & lp_strFieldToEdit & " = " & l_UpdateValue & " WHERE " & lp_strFieldToSearch & " = '" & lp_varCriterea & "'"

Set c = New ADODB.Connection
c.Open g_strConnection

c.Execute l_strSQL
c.Close
Set c = Nothing

End Function

Public Function GetNextSequence(lp_strSequenceName As String) As Long
    
    'TVCodeTools ErrorEnablerStart
    On Error GoTo PROC_ERR
    'TVCodeTools ErrorEnablerEnd
    
    Dim c As ADODB.Connection
    Dim l_strSQL As String
    Dim l_lngGetNextSequence  As Long
    Dim l_rsSequence As ADODB.Recordset
    
    Set c = New ADODB.Connection
    c.Open g_strConnection
    
    l_strSQL = "SELECT * FROM sequence WHERE sequencename = '" & lp_strSequenceName & "';"
    
    Set l_rsSequence = New ADODB.Recordset
    l_rsSequence.Open l_strSQL, c, adOpenDynamic, adLockBatchOptimistic
    
    If Not l_rsSequence.EOF Then
        l_rsSequence.MoveFirst
        l_lngGetNextSequence = l_rsSequence("sequencevalue")
        
        l_strSQL = "UPDATE sequence SET sequencevalue = '" & l_lngGetNextSequence + 1 & "', muser = 'CFM', mdate = getdate() WHERE sequencename = '" & lp_strSequenceName & "';"
        
        c.Execute l_strSQL
    
    End If
    
    l_rsSequence.Close
    Set l_rsSequence = Nothing
    c.Close
    Set c = Nothing
    
    GetNextSequence = l_lngGetNextSequence
    
    
    'TVCodeTools ErrorHandlerStart
PROC_EXIT:
    Exit Function
    
PROC_ERR:
    App.LogEvent Err.Description
    Resume PROC_EXIT
    'TVCodeTools ErrorHandlerEnd
    
End Function

Function QuoteSanitise(lp_strText) As String
        '<EhHeader>
        On Error GoTo QuoteSanitise_Err
        '</EhHeader>


        Dim l_strNewText As String

        Dim l_strOldText As String

        Dim l_intLoop    As Integer

         l_strNewText = ""
    
         If IsNull(lp_strText) Then
            l_strOldText = ""
            l_strNewText = l_strOldText
            lp_strText = ""
        Else
            l_strOldText = lp_strText
        End If
    
        If InStr(lp_strText, "'") <> 0 Then
        
            For l_intLoop = 1 To Len(l_strOldText)
            
                If Mid(l_strOldText, l_intLoop, 1) = "'" Then
                    l_strNewText = l_strNewText & "''"
                Else
                    l_strNewText = l_strNewText & Mid(l_strOldText, l_intLoop, 1)
                End If
            
            Next

        Else
            l_strNewText = lp_strText
        End If
    
        If Right(l_strNewText, 1) = "/" Or Right(l_strNewText, 1) = "\" Then
            l_strNewText = Left(l_strNewText, Len(l_strNewText) - 1)
        End If
    
        QuoteSanitise = l_strNewText
    

        '<EhFooter>
        Exit Function

QuoteSanitise_Err:
        App.LogEvent Err.Description & vbCrLf & _
               "in prjJCAFileManager.modTest.QuoteSanitise " & _
               "at line " & Erl
        Resume Next
        '</EhFooter>
End Function

Sub SendSMTPMail(ByVal lp_strEmailToAddress As String, ByVal lp_strEmailToName As String, ByVal lp_strSubject As String, ByVal lp_strBody As String, ByVal lp_strAttachment As String, _
ByVal lp_blnDontShowErrors As Boolean, ByVal lp_strCopyToEmailAddress As String, ByVal lp_strCopyToName As String, Optional ByVal lp_strBCCAddress As String, _
Optional lp_blnNoGlobalFooter As Boolean, Optional lp_strEmailFromAddress As String)

Dim SQL As String, cnn2 As ADODB.Connection

If lp_blnNoGlobalFooter = True Then
    'Nothing
Else
    lp_strBody = lp_strBody & vbCrLf & vbCrLf & g_strEmailFooter
End If

SQL = "INSERT INTO emailmessage (emailTo, emailCC, emailBCC, emailSubject, emailBodyPlain, emailAttachmentPath, emailFrom, cuser, muser, SendViaService) VALUES ("
SQL = SQL & "'" & QuoteSanitise(lp_strEmailToAddress) & "', "
SQL = SQL & IIf(lp_strCopyToEmailAddress <> "", "'" & QuoteSanitise(lp_strCopyToEmailAddress) & "', ", "Null, ")
SQL = SQL & IIf(lp_strBCCAddress <> "", "'" & QuoteSanitise(lp_strBCCAddress) & "', ", "Null, ")
SQL = SQL & "'" & QuoteSanitise(lp_strSubject) & "', "
SQL = SQL & "'" & QuoteSanitise(lp_strBody) & "', "
SQL = SQL & IIf(lp_strAttachment <> "", "'" & QuoteSanitise(lp_strAttachment) & "', ", "Null, ")
SQL = SQL & IIf(lp_strEmailFromAddress <> "", "'" & QuoteSanitise(lp_strEmailFromAddress) & "', ", "Null, ")
SQL = SQL & "'Aspera', "
SQL = SQL & "'Aspera', "
SQL = SQL & "1);"

'On Error Resume Next
Debug.Print SQL
Set cnn2 = New ADODB.Connection
cnn2.Open g_strConnection
cnn2.Execute SQL
cnn2.Close
Set cnn2 = Nothing
'On Error GoTo 0

End Sub

Public Function FormatSQLDate(lDate As Variant)
    
    'TVCodeTools ErrorEnablerStart
    On Error GoTo PROC_ERR
    'TVCodeTools ErrorEnablerEnd
    
    If IsNull(lDate) Then
        FormatSQLDate = Null
        Exit Function
    End If
    If lDate = "" Then
        FormatSQLDate = Null
        Exit Function
    End If
    If Not IsDate(lDate) Then
        FormatSQLDate = Null
        Exit Function
    
    End If
    
    FormatSQLDate = Year(lDate) & "/" & Month(lDate) & "/" & Day(lDate) & " " & Hour(lDate) & ":" & Minute(lDate) & ":" & Second(lDate)
    
    'TVCodeTools ErrorHandlerStart
PROC_EXIT:
    Exit Function
    
PROC_ERR:
    App.LogEvent Err.Description
    Resume PROC_EXIT
    'TVCodeTools ErrorHandlerEnd
    
End Function

Function Check_Reference(lp_strText As String) As Boolean

Dim l_expValidReferenceExpression As RegExp
Set l_expValidReferenceExpression = New RegExp
l_expValidReferenceExpression.Pattern = "^[a-zA-Z0-9_\s-]+$"
If l_expValidReferenceExpression.Test(lp_strText) = True Then
    Check_Reference = True
Else
    Check_Reference = False
End If

End Function

Function IsItHere(lp_strLocation As String) As Boolean

Dim l_blnIsIt As Boolean, l_varTemp As Variant
l_blnIsIt = False

If IsNumeric(lp_strLocation) Then
    
    'Numeric locations are job numbers - it is booked out to a job number - this is an internal location
    l_blnIsIt = True

Else
    
    l_varTemp = GetDataSQL("SELECT format FROM xref WHERE category = 'location' and description = '" & lp_strLocation & "';")
    Select Case l_varTemp
        'List of locations that are defined as 'Here' - not including the ones that will be going away
        Case "ONSITE"
            l_blnIsIt = True
        Case Else
            l_blnIsIt = False
    
    End Select

End If

IsItHere = l_blnIsIt

End Function

Function GetDataSQL(lp_strSQL As String) As Variant
        '<EhHeader>
        On Error GoTo GetData_Err
        '</EhHeader>


    Dim l_rstGetData As New ADODB.Recordset
    Dim l_con As New ADODB.Connection

    l_con.Open g_strConnection
    l_rstGetData.Open lp_strSQL, l_con, adOpenStatic, adLockReadOnly

    If l_rstGetData.RecordCount > 0 Then
        If Not IsNull(l_rstGetData.Fields(0)) Then
            l_rstGetData.MoveFirst
            GetDataSQL = l_rstGetData.Fields(0)
            GoTo Proc_CloseDB
        
        End If
    Else
        GoTo Proc_CloseDB
    End If

    Select Case l_rstGetData.Fields(0).Type
    Case adChar, adVarChar, adVarWChar, 201, 203
        GetDataSQL = ""
    Case adBigInt, adBinary, adBoolean, adCurrency, adDecimal, adDouble, adInteger
        GetDataSQL = 0
    Case adDate, adDBDate, adDBTime, adDBTimeStamp
        GetDataSQL = 0
    Case Else
        GetDataSQL = False
    End Select

Proc_CloseDB:

    On Error Resume Next

    l_rstGetData.Close
    Set l_rstGetData = Nothing

    l_con.Close
    Set l_con = Nothing


        '<EhFooter>
        Exit Function

GetData_Err:
        App.LogEvent Err.Description & vbCrLf & _
               "in prjJCAFileManager.modTest.GetData " & _
               "at line " & Erl
        Resume Next
        '</EhFooter>
End Function

Function GetStatusNumber(lp_strStatus As String) As Integer
    'TVCodeTools ErrorEnablerStart
    On Error GoTo PROC_ERR
    'TVCodeTools ErrorEnablerEnd
    
    Dim l_intStatusNumber As Integer
    Select Case LCase(lp_strStatus)
        Case "pencil"
            l_intStatusNumber = 0
        Case "submitted"
            l_intStatusNumber = 1
        Case "confirmed"
            l_intStatusNumber = 2
        Case "despatched"
            l_intStatusNumber = 2
        Case "prepared"
            l_intStatusNumber = 2
        Case "masters here"
            l_intStatusNumber = 3
        Case "scheduled"
            l_intStatusNumber = 4
        Case "in progress"
            l_intStatusNumber = 5
        Case "pending"
            l_intStatusNumber = 6
        Case "vt done"
            l_intStatusNumber = 7
        Case "completed"
            l_intStatusNumber = 8
        Case "hold cost"
            l_intStatusNumber = 9
        Case "costed"
            l_intStatusNumber = 10
        Case "sent to accounts", "no charge"
            l_intStatusNumber = 11
    End Select
    GetStatusNumber = l_intStatusNumber
    
    'TVCodeTools ErrorHandlerStart
PROC_EXIT:
    Exit Function
    
PROC_ERR:
    MsgBox Err.Description
    Resume PROC_EXIT
    'TVCodeTools ErrorHandlerEnd
    
End Function

Function GetCount(lp_strSQL As String) As Double

Screen.MousePointer = vbHourglass

Dim l_conSearch As ADODB.Connection
Dim l_rstSearch As ADODB.Recordset

Set l_conSearch = New ADODB.Connection
Set l_rstSearch = New ADODB.Recordset

l_conSearch.ConnectionString = g_strConnection
l_conSearch.Open

With l_rstSearch
     .CursorLocation = adUseClient
     .LockType = adLockBatchOptimistic
     .CursorType = adOpenDynamic
     .Open lp_strSQL, l_conSearch, adOpenStatic, adLockReadOnly
End With

l_rstSearch.ActiveConnection = Nothing

If l_rstSearch.EOF Then
    GetCount = 0
Else

    If Not IsNull(l_rstSearch(0)) Then
        GetCount = Val(l_rstSearch(0))
    Else
        GetCount = 0
    End If
End If

l_rstSearch.Close
Set l_rstSearch = Nothing

l_conSearch.Close
Set l_conSearch = Nothing

Screen.MousePointer = vbDefault

End Function

Function SanitiseBarcode(lp_strBarcode As String) As String
    
Dim l_strNewBarcode As String
l_strNewBarcode = Replace(lp_strBarcode, "/", "-")
l_strNewBarcode = Replace(l_strNewBarcode, "\", "-")
l_strNewBarcode = Replace(l_strNewBarcode, "?", "-")
l_strNewBarcode = Replace(l_strNewBarcode, "*", "-")
l_strNewBarcode = Replace(l_strNewBarcode, "&", "-")
l_strNewBarcode = Replace(l_strNewBarcode, "#", "-")
'l_strNewBarcode = Replace(l_strNewBarcode, "%", "-")

SanitiseBarcode = l_strNewBarcode

Exit Function
    
End Function

Function DADCSanitiseBarcode(lp_strBarcode As String) As String
    
Dim l_strNewBarcode As String
l_strNewBarcode = Replace(lp_strBarcode, "/", "_")
l_strNewBarcode = Replace(l_strNewBarcode, "\", "_")
l_strNewBarcode = Replace(l_strNewBarcode, "?", "_")
l_strNewBarcode = Replace(l_strNewBarcode, "*", "_")
l_strNewBarcode = Replace(l_strNewBarcode, "&", "_")
l_strNewBarcode = Replace(l_strNewBarcode, "#", "_")
'l_strNewBarcode = Replace(l_strNewBarcode, "%", "-")

DADCSanitiseBarcode = l_strNewBarcode

Exit Function
    
End Function


